#!/usr/bin/env python3

# Copyright Louis Paternault 2017
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from PIL import Image

COLORS = {
    0: (255, 255, 255),
    1: (0, 0, 0),
    }

def usage():
    print("USAGE: img2pixelart FILE")
    print(r"Convert image to a text that can be given as argument to the \bwpixelart LaTeX command of the pixelart package.")

def distance(color1, color2):
    """Return the distance between two colors.

    Two close images have a low distance.
    """
    return sum((
        (color1[i]-color2[i])**2
        for i in range(3)
        ))

def closest(color):
    """Return the closest color (among colors of COLORS) to the argument.
    """
    closestkey = 0
    closestdistance = 3**255**2
    for key, rgb in COLORS.items():
        if distance(rgb, color) < closestdistance:
            closestkey = key
            closestdistance = distance(rgb, color)
    return closestkey


def img2pixelart(imagename):
    """Print stream of 0s and 1s representing the image."""
    with Image.open(imagename).convert("RGB") as image:
        size = image.size
        pixels = image.load()
        for y in range(size[1]):
            for x in range(size[0]):
                print(closest(pixels[x, y]), end="")
            print()

if __name__ == "__main__":
    if len(sys.argv) == 2:
        img2pixelart(sys.argv[1])
        sys.exit(0)
    usage()
    sys.exit(1)
