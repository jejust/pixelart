#!/usr/bin/env python

# This file is part of Pixelart.
#
# Pixelart is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pixelart is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Pixelart.  If not, see <http://www.gnu.org/licenses/>.

"""Generate some pixel-art pictures.

Usage:

    $ generate.py mono 256

    $ generate.py concentric 256

    $ generate.py different 256
"""

import sys
import random

def pattern_mono(size):
    for _ in range(size):
        print("1" * size)

def _randchar(k=1):
    return "".join(random.sample("0123456789", k))

def pattern_concentric(size):
    if size % 2 == 0:
        lines = ["..", ".."]
    else:
        lines = [_randchar()]

    for _ in range(size // 2):
        char = _randchar()
        lines = [
                char * (len(lines[0]) + 2),
                ] + list(
                        char + line + char
                        for line
                        in lines
                        ) + [
                char * (len(lines[0]) + 2),
                ]
    print("\n".join(lines))

def pattern_different(size):
    line = _randchar(4)
    for i in range(0, 2*size, 2):
        print(((line[(i%4):] + line[:(i%4)]) * (size//4 + 1))[0:size])

PATTERNS = {
        "mono": pattern_mono,
        "concentric": pattern_concentric,
        "different": pattern_different,
        }

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(__doc__.strip())
        sys.exit(1)
    PATTERNS[sys.argv[1]](int(sys.argv[2]))
