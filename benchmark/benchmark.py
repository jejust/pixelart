#!/usr/bin/env python

# Copyright 2022 Louis Paternault
#
# This file is part of Pixelart.
#
# Pixelart is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pixelart is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Pixelart.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import pathlib
import subprocess
import sys

REPEAT = 10

def lualatex(path):
    subprocess.run(
            ("lualatex", path.stem),
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            cwd=path.parent,
            check=True,
            )

def benchmark(path):
    # Warmup
    lualatex(path)

    start = datetime.datetime.now()
    for _ in range(REPEAT):
        lualatex(path)

    print(f"{path.stem}	{(datetime.datetime.now()-start).total_seconds()/REPEAT}")

if __name__ == "__main__":
    if sys.argv == 1:
        files = sorted(pathlib.Path(__file__).parent.glob("*.tex"))
    else:
        files = (pathlib.Path(arg) for arg in sys.argv[1:])

    for filename in files:
        benchmark(filename)
